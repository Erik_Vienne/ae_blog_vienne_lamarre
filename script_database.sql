
create database if not exists blogjavaee default character set utf8 collate utf8_general_ci;

user blogjavaee;

DROP TABLE user IF EXISTS;
CREATE TABLE `user` (
	`user_id` INT NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) NOT NULL,
	`password` VARCHAR(256) NOT NULL,
	PRIMARY KEY (`user_id`)
);

DROP TABLE friends IF EXISTS;
CREATE TABLE `friends` (
	`user_id_1` INT NOT NULL,
	`user_id_2` INT NOT NULL,
	`status` INT NOT NULL
);

DROP TABLE post IF EXISTS;
CREATE TABLE `post` (
	`post_id` INT NOT NULL AUTO_INCREMENT,
	`content` TEXT NOT NULL,
	`author_id` INT NOT NULL,
	`date` DATE NOT NULL,
	PRIMARY KEY (`post_id`)
);

DROP TABLE comment IF EXISTS;
CREATE TABLE `comment` (
	`comment_id` INT NOT NULL AUTO_INCREMENT,
	`post_id` INT NOT NULL,
	`content` TEXT NOT NULL,
	`author_id` INT NOT NULL,
	`date` DATE NOT NULL,
	PRIMARY KEY (`comment_id`)
);