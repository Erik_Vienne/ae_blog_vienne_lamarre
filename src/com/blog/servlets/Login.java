package com.blog.servlets;


import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.blog.bdd.Users;
import com.blog.beans.User;
import com.blog.beans.Password;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") != null) {
			response.sendRedirect(request.getContextPath() + "/home");
		} else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("user") != null) {
			response.sendRedirect(request.getContextPath() + "/home");
		} else {
			String username = request.getParameter("username");
			String pwd = request.getParameter("password");
			
			Users users = new Users();
			
			User current = users.getUserByName(username);
			
			
			if(current != null) {
				try {
					if(current.getPassword().equals(Password.toHexString(Password.getSHA(pwd)))) {
						session.setAttribute("user", current);
						session.setAttribute("username", username);
						response.sendRedirect(request.getContextPath() + "/home");
					}
					else {
						request.setAttribute("erreur", "Mot de passe incorrect");
						doGet(request, response);
					}
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else {
				request.setAttribute("erreur", "Identifiant incorrect");
				doGet(request, response);
			}
		}
		
		
		
	}

}
