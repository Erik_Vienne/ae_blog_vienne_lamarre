package com.blog.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.blog.beans.Password;
import com.blog.beans.User;
import com.blog.bdd.Users;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("user") != null) {
			response.sendRedirect(request.getContextPath() + "/home");
		} else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") != null) {
			response.sendRedirect(request.getContextPath() + "/home");
		} else {
			PrintWriter out = response.getWriter();
			String username = request.getParameter("username");
			String pwd = request.getParameter("password");
			String confirm_pwd = request.getParameter("confirm_password");
			
			Users users = new Users();
			
			if(!users.usernameExist(username)) {
				if(pwd.equals(confirm_pwd) && !pwd.equals("")) {
					try {
						String hash_pwd = Password.toHexString(Password.getSHA(pwd));
						users.addUser(new User(username, hash_pwd));
						
						User current = users.getUserByName(username);
						session.setAttribute("user", current);
						session.setAttribute("username", username);
						
						response.sendRedirect(request.getContextPath() + "/");
						
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else {
					request.setAttribute("erreur", "Les mots de passes ne correspondent pas");
					doGet(request, response);
				}
			}
			else {
				request.setAttribute("erreur", "Pseudo d�j� utilis�");
				doGet(request, response);
			}
		}
		// TODO Auto-generated method stub
		
	}
}
