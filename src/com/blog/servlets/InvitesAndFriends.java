package com.blog.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.blog.bdd.Users;
import com.blog.beans.User;

/**
 * Servlet implementation class InvitesAndFriends
 */
@WebServlet("/InvitesAndFriends")
public class InvitesAndFriends extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InvitesAndFriends() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		Users users = new Users();
		User current = (User) session.getAttribute("user");
		
		if(session.getAttribute("user") != null) {	
			request.setAttribute("invitationsent", users.InvitationSent(current.getId()));
			request.setAttribute("invitationreceived", users.InvitationReceived(current.getId()));
			request.setAttribute("friendslist", users.friendsList(current.getId()));
			this.getServletContext().getRequestDispatcher("/WEB-INF/invitesandfriends.jsp").forward(request, response);
		}
		else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
