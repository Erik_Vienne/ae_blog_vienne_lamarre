package com.blog.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.blog.bdd.Posts;
import com.blog.bdd.Users;
import com.blog.beans.Comment;
import com.blog.beans.Post;
import com.blog.beans.User;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("user") != null) {
			if(request.getParameter("id") != null) {
				try {
					int id = Integer.parseInt(request.getParameter("id"));
					request.setAttribute("profile_id", id);
					Users users = new Users();
					request.setAttribute("profile", users.getUserById(id));
					User current = (User) session.getAttribute("user");
					if(current.getId() == id) {
						request.setAttribute("isCurrent", true);
					} else {
						request.setAttribute("isCurrent", false);
						int friends =  users.isFriend(current.getId(), id);
						request.setAttribute("friends", friends);
						if(friends != -1) {
							request.setAttribute("sender", users.isSender(current.getId(), id));
						}
					}
					
					this.getServletContext().getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
				} catch(Exception e) {
					response.sendRedirect(request.getContextPath() + "/home");
				}
				
			
			}
			else {
				response.sendRedirect(request.getContextPath() + "/home");
			}
			
		}
		else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		Users users = new Users();
		Posts posts = new Posts();
		User current = (User) session.getAttribute("user");
		
		if(request.getParameter("id_other") != null) {
			int ask = Integer.parseInt(request.getParameter("id_other"));
			users.askFriend(current.getId(), ask);	
		}
		if(request.getParameter("accept") != null) {
			int accept = Integer.parseInt(request.getParameter("accept"));
			users.acceptFriend(current.getId(), accept);
		}
		if (request.getParameter("deny") != null) {
			int deny = Integer.parseInt(request.getParameter("deny"));
			users.denyFriend(current.getId(), deny);
		}
		if (request.getParameter("add_comment") != null) {
			
			int post_id = Integer.parseInt(request.getParameter("post_id"));
			int author_id = Integer.parseInt(request.getParameter("author_id"));
			String content = request.getParameter("content");
			Date now = new Date();
			
			Comment comment = new Comment(post_id, author_id, content, now);
			posts.addComment(comment);
		}
		doGet(request, response);
	}

}
