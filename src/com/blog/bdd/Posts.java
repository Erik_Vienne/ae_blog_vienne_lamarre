package com.blog.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.blog.beans.Comment;
import com.blog.beans.Post;
import com.blog.beans.User;

public class Posts {
	
	private Connection connexion;
	
	private void loadDatabase() {
        // Chargement du driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        }

        try {
            connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/blogjavaee?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
	public void addPost(Post post) {
        loadDatabase();
        
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO post(content, author_id, date) VALUES(?, ?, ?);");
            preparedStatement.setString(1, post.getContent());
            preparedStatement.setInt(2, post.getAuthor_id());
            preparedStatement.setDate(3, new java.sql.Date(post.getDate().getTime()));
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
	public List<Post> getPostsFromUser(int id){
		loadDatabase();
		List<Post> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM post where author_id=(?);");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();

            while(resultat.next()) {
            	Post post = new Post();
            	post.setId(resultat.getInt("post_id"));
            	post.setAuthor_id(resultat.getInt("author_id"));
            	post.setContent(resultat.getString("content"));
            	post.setDate(resultat.getDate("date"));
            	res.add(post);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
		return res;
	}
	
	public List<Post> getAccessiblePosts(int user_id){
		loadDatabase();
		List<Post> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM post where author_id=(?) or"
        			+ " author_id in(select user_id_1 from friends where user_id_2=(?) and status=1) or"
        			+ " author_id in(select user_id_2 from friends where user_id_1=(?) and status=1) order by date desc;");
            preparedStatement.setInt(1, user_id);
            preparedStatement.setInt(2, user_id);
            preparedStatement.setInt(3, user_id);
            resultat = preparedStatement.executeQuery();

            while(resultat.next()) {
            	Post post = new Post();
            	post.setId(resultat.getInt("post_id"));
            	post.setAuthor_id(resultat.getInt("author_id"));
            	post.setContent(resultat.getString("content"));
            	post.setDate(resultat.getDate("date"));
            	res.add(post);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
		return res;
	}
	
	public User getPostAuthor(int id){
		loadDatabase();
		User user = null;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM user where user_id=?;");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();

            while(resultat.next()) {
            	user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
		return user;
	}
	
	public void addComment(Comment comment) {
loadDatabase();
        
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO comment(post_id, content, author_id, date) VALUES(?, ?, ?, ?);");
            preparedStatement.setInt(1, comment.getPost_id());
            preparedStatement.setString(2, comment.getContent());
            preparedStatement.setInt(3, comment.getAuthor_id());
            preparedStatement.setDate(4, new java.sql.Date(comment.getDate().getTime()));
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
	}
	
	public List<Comment> getComments(int post_id){
		loadDatabase();
		List<Comment> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM comment where post_id=(?) order by date desc;");
            preparedStatement.setInt(1, post_id);
            resultat = preparedStatement.executeQuery();

            while(resultat.next()) {
            	Comment comment = new Comment();
            	comment.setAuthor_id(resultat.getInt("author_id"));
            	comment.setComment_id(resultat.getInt("comment_id"));
            	comment.setContent(resultat.getString("content"));
            	comment.setDate(resultat.getDate("date"));
            	comment.setPost_id(resultat.getInt("post_id"));
            	res.add(comment);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
		return res;
	}

}
