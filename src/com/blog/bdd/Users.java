package com.blog.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

import com.blog.beans.User;

public class Users {
	
	private Connection connexion;

	
	private void loadDatabase() {
        // Chargement du driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        }

        try {
            connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/blogjavaee?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
	public void addUser(User user) {
        loadDatabase();
        
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO user(username, password) VALUES(?, ?);");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
	public boolean usernameExist(String username) {
		loadDatabase();
		int count = 0;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM user where username = (?);");
            preparedStatement.setString(1, username);
            resultat = preparedStatement.executeQuery();

            

            while(resultat.next()) {
            	count++;
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return count!=0;
	}
	
	public User getUserByName(String username) {
		loadDatabase();
		User user = null;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM user where username = (?);");
            preparedStatement.setString(1, username);
            resultat = preparedStatement.executeQuery();

            if(resultat.next()) {
            	user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            	user.setPassword(resultat.getString("password"));
            }
            
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return user;
	}
	
	public User getUserById(int id) {
		loadDatabase();
		User user = null;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM user where user_id = (?);");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();

            if(resultat.next()) {
            	user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            }
            
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return user;
	}
	
	public List<User> searchUsers(String username){
		loadDatabase();
		List<User> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM user where username like (?) order by username;");
            preparedStatement.setString(1, "%" + username + "%");
            resultat = preparedStatement.executeQuery();

            while(resultat.next()) {
            	User user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            	res.add(user);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	public int isFriend(int current, int other) {
		loadDatabase();
		int res = -1;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM friends where (user_id_1= (?) and user_id_2 = (?)) or (user_id_2= (?) and user_id_1 = (?));");
            preparedStatement.setInt(1, current);
            preparedStatement.setInt(2, other);
            preparedStatement.setInt(3, current);
            preparedStatement.setInt(4, other);
            resultat = preparedStatement.executeQuery();

            

            if(resultat.next()) {
        		res = resultat.getInt("status");
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	public List<User> InvitationSent(int current) {
		loadDatabase();
		List<User> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("select * from user where user_id in (SELECT user_id_2 FROM friends where user_id_1= (?) and status=0) order by username;");
            preparedStatement.setInt(1, current);
            resultat = preparedStatement.executeQuery();

            

            while(resultat.next()) {
            	User user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            	res.add(user);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	public List<User> InvitationReceived(int current) {
		loadDatabase();
		List<User> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;

        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("select * from user where user_id in (SELECT user_id_1 FROM friends where user_id_2= (?) and status=0) order by username;");
            preparedStatement.setInt(1, current);
            resultat = preparedStatement.executeQuery();

            

            while(resultat.next()) {
            	User user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            	res.add(user);
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	public List<User> friendsList(int current) {
		loadDatabase();
		List<User> res = new ArrayList<>();
		Statement statement = null;
        ResultSet resultat = null;
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("select * from user where user_id in (select user_id_2 from friends where user_id_1=? and status=1) or user_id in (select user_id_1 from friends where user_id_2=? and status=1);");
            preparedStatement.setInt(1, current);
            preparedStatement.setInt(2, current);
            resultat = preparedStatement.executeQuery();
            
            while(resultat.next()) {
            	User user = new User();
            	user.setId(resultat.getInt("user_id"));
            	user.setUsername(resultat.getString("username"));
            	res.add(user);
            }
            
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	
	public void askFriend(int current, int other) {
		loadDatabase();
        
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("INSERT INTO friends(user_id_1, user_id_2, status) VALUES(?, ?, 0);");
            preparedStatement.setInt(1, current);
            preparedStatement.setInt(2, other);
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	public boolean isSender(int current, int other) {
		loadDatabase();
		boolean res = false;
		Statement statement = null;
        ResultSet resultat = null;

        loadDatabase();
        
        try {
        	PreparedStatement preparedStatement = connexion.prepareStatement("SELECT * FROM friends where user_id_1= (?) and user_id_2 = (?);");
            preparedStatement.setInt(1, current);
            preparedStatement.setInt(2, other);
            resultat = preparedStatement.executeQuery();
            
            if(resultat.next()) {
        		res = true;
            }
        } catch (SQLException e) {
        } finally {
            // Fermeture de la connexion
            try {
                if (resultat != null)
                    resultat.close();
                if (statement != null)
                    statement.close();
                if (connexion != null)
                    connexion.close();
            } catch (SQLException ignore) {
            }
        }
        
		return res;
	}
	
	public void acceptFriend(int current, int other) {
		
		loadDatabase();
        
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("update friends set status=1 where user_id_1= (?) and user_id_2 = (?);");
            preparedStatement.setInt(1, other);
            preparedStatement.setInt(2, current);
            
            preparedStatement.executeUpdate();
            
            connexion.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
	}
	
	public void denyFriend(int current, int other) {
		
		loadDatabase();
		
        try {
            PreparedStatement preparedStatement = connexion.prepareStatement("delete from friends where user_id_1=? and user_id_2=?;");
            preparedStatement.setInt(1, other);
            preparedStatement.setInt(2, current);
            
            preparedStatement.executeUpdate();

            connexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
}
	
