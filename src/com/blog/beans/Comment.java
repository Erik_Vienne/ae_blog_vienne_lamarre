package com.blog.beans;

import java.util.Date;

public class Comment {
	private int comment_id, post_id, author_id;
	private String content;
	private Date date;
	
	public Comment() {}
	

	public Comment(int post_id, int author_id, String content, Date date) {
		this.post_id = post_id;
		this.author_id = author_id;
		this.content = content;
		this.date = date;
	}


	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}

	public int getPost_id() {
		return post_id;
	}

	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}

	public int getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(int author_id) {
		this.author_id = author_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
