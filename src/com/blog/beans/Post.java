package com.blog.beans;

import java.util.Date;

public class Post {
	private int id, author_id;
	private String content;
	private Date date;
	
	public Post() {}
	
	public Post(String content, int author_id, Date date) {
		this.content = content;
		this.author_id = author_id;
		this.date = date;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAuthor_id() {
		return author_id;
	}
	public void setAuthor_id(int author_id) {
		this.author_id = author_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
