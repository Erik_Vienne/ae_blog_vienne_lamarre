package com.blog.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class Protection
 */
@WebFilter("/*")
public class Protection implements Filter {

    /**
     * Default constructor. 
     */
    public Protection() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
			
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			HttpSession session = request.getSession();
			
			String contextPath = request.getServletContext().getContextPath();
			String newIp = request.getRemoteAddr();
			String newUserAgent = request.getHeader("User-Agent");
			String oldIp = (String) session.getAttribute("User-Ip");
			String oldUserAgent = (String) session.getAttribute("User-Agent");
			
			try {
			    if (!newIp.equals(oldIp)) {
				session.setAttribute("User-Ip", newIp);
			    }
			    if (!newUserAgent.equals(oldUserAgent)) {
				session.setAttribute("User-Agent", newUserAgent);
			    }
			    chain.doFilter(request, response);
			} catch (IllegalStateException e) {
			    response.sendRedirect(contextPath + "/login");
			}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
