
INSTALLATION DU PROJET :

 - copier le dossier du projet dans le dossier des projets eclipse

 - Importer le projet dans eclipse :
 		- File > Import > General > Existing Project Into Workspace

 - Executer le script script_database.sql dans une base de donn�es mysql.
 
 - Dans les fichiers Posts.java et Users.java du package com.blog.bdd, dans la m�thode loadDatabase(), adapter les parem�tres � votre utilisateur tel que :
 			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/blogjavaee?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "utilisateur", "motdepasse");
 	ou changer le mot de passe de l'utilisateur root pour que ce soit : root
 
 - Configurer le projet sur le serveur Apache tomcat 9.0
 
 - Lancer la Servlet "Home" pour commencer � utiliser le projet
