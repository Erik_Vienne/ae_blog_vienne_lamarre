<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<%
User u = (User) request.getAttribute("profile");
%>

<head>
<meta charset="utf8">
<title>Page de <% out.print(u.getUsername()); %></title>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="mx-auto" style="width: 200px;">
  <h1><% out.print(u.getUsername()); %></h1>
</div>

	
	<%
	if ( (request.getAttribute("friends") != null && (Integer) request.getAttribute("friends") == 1) || (boolean) request.getAttribute("isCurrent")){

	Posts posts = new Posts();
	Users users = new Users();
	int profile_id = (Integer) request.getAttribute("profile_id");
	List<Post> postsList = posts.getPostsFromUser(profile_id);
	%>
	
	<%
	for (Post p : postsList) {
		User author = (User) posts.getPostAuthor(p.getAuthor_id());
	%>
	<div class="card" style="margin: 2em;">
	  <div class="card-body">
	    
	    <h5 class="card-title">Post par <%out.print(author.getUsername());%> le <% out.print(p.getDate().toString()); %></h5>
	    	    <% if((boolean) request.getAttribute("isCurrent")){ %>
	    <a href="#" class="card-link">Supprimer le post</a>
	    <% } 
	    %>
	    <p class="card-text"><% out.print(p.getContent()); %></p>	    

		<p>
		  <a class="card-link" data-toggle="collapse" href="#comment_<% out.print(p.getId()); %>" role="button" aria-expanded="false" aria-controls="comment_<% out.print(p.getId()); %>">
		    Commenter
		  </a>
		</p>
		<div class="collapse" id="comment_<% out.print(p.getId()); %>">
		  <div class="card card-body">
		  <div>
		  <form action="" method="post">
		  <textarea rows = "5" cols = "60" name="content"/></textarea>
		  <input type="hidden" name="post_id" value="<% out.print(p.getId());%>">
		  <input type="hidden" name="author_id" value="<% out.print(p.getAuthor_id());%>">
		  <input type="submit" class="btn btn-primary" name="add_comment" value="Publier le commentaire"/>
		  </form>
		  </div>
		    -----
		    <%
		    List<Comment> comments = posts.getComments(p.getId());
		    for(Comment c : comments){
		    	User comment_author = users.getUserById(c.getAuthor_id());
		    %>
		    <div class="card" style="margin-top : 1em;">
			  <div class="card-body">
			    <h5 class="card-title">Commentaire de <% out.print(comment_author.getUsername()); %> le <% out.print(c.getDate().toString()); %></h5>
			    <p class="card-text"><% out.print(c.getContent()); %></p>
			  </div>
			</div>
			<% } %>
		    
		  </div>
		</div>
	    
	   
	    
	  </div>
	</div>
		
	<% } } else if((int) request.getAttribute("friends") == 0){
		if((boolean) request.getAttribute("sender")){
		%>
		<p> Demande d'amis envoy�e </p>
		<% } else{ %>
		<form action="" method="post" class="form-inline">
		<button type="submit" name="accept" value="<%out.print(u.getId());%>" class="btn btn-primary">Accepter</button>
		<button type="submit" name="deny" value="<%out.print(u.getId());%>" class="btn btn-primary">Refuser</button>
		</form>
	<% } 
		} else {%>
	<form action="" method="post" class="form-inline">
	<button type="submit" name="id_other" value="<%out.print(u.getId());%>" class="btn btn-primary">Ajouter aux amis</button>
	</form>
<% }
	%>

</body>
</html>