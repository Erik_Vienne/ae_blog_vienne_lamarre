<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>S'inscrire</title>
</head>
<body>
<form action="" method="post">
<div>
<p>pseudo :</p>
<input type="text" name="username" required>
</div>
<div>
<p>mot de passe :</p>
<input type="password" name="password" required>
</div>
<div>
<p>confirmer mot de passe :</p>
<input type="password" name="confirm_password" required>
</div>
<button type="submit" >S'inscrire</button>
</form>

<p>${ empty erreur ? '' : erreur }</p>

<a href="./login">D�j� un compte ? Connectez vous ici</a>
</body>
</html>