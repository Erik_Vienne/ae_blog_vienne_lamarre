<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Recherche</title>
</head>
<body>
<%@ include file="header.jsp" %>



<%
	List<User> list = (List) request.getAttribute("result");
%>
 <div class="list-group">
 <form action="./profile" method="get">
<%
	for (User u : list) {
%>
	<button type="submit" name="id" value="<%out.print(u.getId());%>" class="list-group-item list-group-item-action">
	<%=u.getUsername()%>
	</button>
<%
	}
%>
</form>
</div>

</body>
</html>