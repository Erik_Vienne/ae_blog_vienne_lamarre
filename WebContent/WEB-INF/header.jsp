<%@ page import="java.util.List, com.blog.beans.User, com.blog.beans.Post, com.blog.beans.Comment, com.blog.bdd.Posts, com.blog.bdd.Users" %>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<header>

<%
User current = (User) request.getSession().getAttribute("user");
%>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" >Bonjour ${ sessionScope.username }</a>
	
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	<ul class="navbar-nav mr-auto">
	<li class="nav-item">
        <a class="nav-link" href="./home">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./post">Publier un post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./invitesandfriends">Mes invitations et amis</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./profile?id=<% out.print(current.getId()); %>">Profil</a>
      </li>
	</ul>
	
	
  
	<form class="form-inline my-2 my-lg-0" action="./search" method="get">
		<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="user">
		<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
   </form>
   <a class="nav-link" href="./logout">Se déconnecter</a>
   </div>
</nav>


  </header>