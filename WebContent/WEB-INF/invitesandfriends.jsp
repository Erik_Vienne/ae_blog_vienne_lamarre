<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<%@ include file="header.jsp" %>

<title>Mes invitations et amis</title>
</head>
<body>

<h3>Demandes envoy�es</h3>
<%
	List<User> listsent = (List) request.getAttribute("invitationsent");
%>
 <div class="list-group">
 <form action="./profile" method="get">
<%
	for (User u : listsent) {
%>
	<button type="submit" name="id" value="<%out.print(u.getId());%>" class="list-group-item list-group-item-action">
	<%=u.getUsername()%>
	</button>
<%
	}
%>
</form>
</div>


  <h3>Demandes Re�ues</h3>
  <%
	List<User> listreceived = (List) request.getAttribute("invitationreceived");
%>
 <div class="list-group">
 <form action="./profile" method="get">
<%
	for (User u : listreceived) {
%>
	<button type="submit" name="id" value="<%out.print(u.getId());%>" class="list-group-item list-group-item-action">
	<%=u.getUsername()%>
	</button>
<%
	}
%>
</form>
</div>


  <h3>Mes amis</h3>
  
    <%
	List<User> myfriends = (List) request.getAttribute("friendslist");
%>
 <div class="list-group">
 <form action="./profile" method="get">
<%
	for (User u : myfriends) {
%>
	<button type="submit" name="id" value="<%out.print(u.getId());%>" class="list-group-item list-group-item-action">
	<%=u.getUsername()%>
	</button>
<%
	}
%>
</form>
</div>

</body>
</html>