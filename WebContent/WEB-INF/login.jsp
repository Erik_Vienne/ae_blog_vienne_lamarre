<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Connexion</title>
</head>
<body>
<form action="" method="post">
<div>
<p>pseudo :</p>
<input type="text" name="username">
</div>
<div>
<p>mot de passe :</p>
<input type="password" name="password">
</div>
<button type="submit" >Se connecter</button>
</form>

<p>${ empty erreur ? '' : erreur }</p>

<a href="./register">Pas de compte ? Inscrivez vous ici</a>

</body>
</html>