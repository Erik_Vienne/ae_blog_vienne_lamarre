<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Accueil</title>
</head>
<body>
<%@ include file="header.jsp" %>

<%
	Posts posts = new Posts();
	Users users = new Users();
	int user_id = (Integer) request.getAttribute("user_id");
	List<Post> postsList = posts.getAccessiblePosts(user_id);
	
	for (Post p : postsList) {
		User author = (User) posts.getPostAuthor(p.getAuthor_id());
	%>
	<div class="card" style="margin: 2em;">
	  <div class="card-body">
	    
	    <h5 class="card-title">Post par <%out.print(author.getUsername());%> le <% out.print(p.getDate().toString()); %></h5>
	    
	    <p class="card-text"><% out.print(p.getContent()); %></p>	    

		<p>
		  <a class="card-link" data-toggle="collapse" href="#comment_<% out.print(p.getId()); %>" role="button" aria-expanded="false" aria-controls="comment_<% out.print(p.getId()); %>">
		    Commenter
		  </a>
		</p>
		<div class="collapse" id="comment_<% out.print(p.getId()); %>">
		  <div class="card card-body">
		  <div>
		  <form action="" method="post">
		  <textarea rows = "5" cols = "60" name="content"/></textarea>
		  <input type="hidden" name="post_id" value="<% out.print(p.getId());%>">
		  <input type="hidden" name="author_id" value="<% out.print(p.getAuthor_id());%>">
		  <input type="submit" class="btn btn-primary" name="add_comment" value="Publier le commentaire"/>
		  </form>
		  </div>
		    -----
		    <%
		    List<Comment> comments = posts.getComments(p.getId());
		    for(Comment c : comments){
		    	User comment_author = users.getUserById(c.getAuthor_id());
		    %>
		    <div class="card" style="margin-top : 1em;">
			  <div class="card-body">
			    <h5 class="card-title">Commentaire de <% out.print(comment_author.getUsername()); %> le <% out.print(c.getDate().toString()); %></h5>
			    <p class="card-text"><% out.print(c.getContent()); %></p>
			  </div>
			</div>	
			<% } %>	    
		  </div>
		</div>
	  </div>
	</div>
	<% } %>
</body>
</html>